import { createGlobalStyle } from 'styled-components'
import fonts from '../fonts'

export default createGlobalStyle`

  :root {
    font-size: 60%;
    color: var(--primary);
    font-family: ${fonts.types.description};
  }

  @media (min-width: 700px) {
    :root {
      font-size: 62.5%;
    }
  }

  body {
    font-size: 1.4rem;
  }

`