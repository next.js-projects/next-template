import texts from './texts'
import containers from './containers'

export default [
  containers,
  texts,
]