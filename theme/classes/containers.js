import { createGlobalStyle } from 'styled-components'

export default createGlobalStyle`

  :root {
    --container-padding: 1.6rem;
  }

  .container {
    max-width: 1024px;
    margin-left: auto;
    margin-right: auto;
    padding: var(--container-padding);
  }

`