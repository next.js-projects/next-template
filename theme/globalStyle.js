import { createGlobalStyle } from "styled-components";
import myTheme from './index'

const createRootStyles = () => {
  const colors = myTheme.colors
  const themeKeys = Object.keys(colors)

  return themeKeys.map(tk => {

    const root = ":root" + (tk === "default" ? "" : "." + tk)

    const themeColorsKeys = Object.keys(colors[tk])

    const stringColorsOfTheme = themeColorsKeys.map(tck => "--"+tck+":"+colors[tk][tck]+";").join("\n")

    return [root, "{", stringColorsOfTheme, "}"].join("\n")

  }).join("\n")
  
}

export default createGlobalStyle`

  ${ createRootStyles() }

  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }
  
  html, body, #root {
    height: 100vh;
  }

  body {    
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;

    overflow-x: hidden;
  }

  .overlay {
    height: inherit;
    width: inherit;
  }

  ul { list-style: none; }

  img, picture, video, embed {
    max-width: 100%;
  }
`