/**
  Colors

  Obs: Mostly of the color variables names are from aproximates names.
  Reference: https://www.htmlcsscolor.com
**/

export const colors = {
  mediumOrchid: '#b76fc9',
  killarney: '#4b894f',
  fireBush: '#d6a049',
  mayaBlue: '#68caf0',
  curiousBlue: '#4190d0',
  blackRussian: '#282a2f',

  darkViolet: '#af00db',
  green: '#008000',
  fuelYellow: '#d99533',
  navy: '#001080',
  blue: '#0000ff',
  white: '#ffffff',


  neonPink: '#e96ec3',
  astronaut: '#3d4a6d',
  screaminGreen: '#3df66b',
  blackSqueeze: '#e5e7d8',
  bilobaFlower: '#b187e8',
  blackPearl: '#0e1419',
}


const allThemesColors = {
  default: {

  },

  dark: {
    primary: colors.mayaBlue,
    'primary-bold': colors.curiousBlue,
    secundary: colors.killarney,

    title: colors.mediumOrchid,
    subtitle: colors.fireBush,

    background: colors.blackRussian,
  },

  light: {
    primary: colors.navy,
    'primary-bold': colors.blue,
    secundary: colors.fuelYellow,

    title: colors.darkViolet,
    subtitle: colors.green,

    background: colors.white,
  },

  dracula: {
    primary: colors.blackSqueeze,
    'primary-bold': colors.bilobaFlower,
    secundary: colors.screaminGreen,

    title: colors.neonPink,
    subtitle: colors.astronaut,

    background: colors.blackPearl,
  }
}

export const getThemesName = () => Object.keys(allThemesColors).filter(t => t !== 'default')

export const getColor = (kindColor, themeName) => {
  const colorByTheme = themeName && allThemesColors[themeName] && allThemesColors[themeName][kindColor]
  return colorByTheme || allThemesColors["default"][kindColor]
}

export default allThemesColors
