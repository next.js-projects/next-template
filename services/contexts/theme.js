import React, { useEffect, useState, createContext } from 'react'

export const ThemeContext = createContext()

export default ({ children }) => {

  const [theme, setTheme] = useState('dark')

  const toggleTheme = () => setTheme(theme === 'dark' ? 'light' : 'dark')

  useEffect(() => {
    document.querySelector(':root').classList = theme
  }, [theme])

  return (
    <ThemeContext.Provider 
      value={{theme, toggleTheme, setTheme}}
    >
      {children}
    </ThemeContext.Provider>
  )
}