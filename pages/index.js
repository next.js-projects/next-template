import { useContext } from 'react'

import { ThemeContext } from '../services/contexts/theme'

import Style from './style'

export default () => {

  const context = useContext(ThemeContext)

  return (
    <Style>
      <button onClick={() => context.toggleTheme()}>Change Theme</button>
    </Style>
  )
}