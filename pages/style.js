import styled from 'styled-components'

export default styled.div`
  background-color: var(--background);

  button {
    background-color: var(--primary);
    color: var(--secundary);
  }
`